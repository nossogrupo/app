package com.polijunior.projeto.prontuario;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void vai(View view) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Créditos");
        alertDialog.setMessage("Anna Gabriela\nDouglas Bezerra\nEduardo Muller\nRômulo Carvalho");
        alertDialog.show();
    }

    public void vaiCadastro(View view) {
        Intent intent = new Intent(this, Teste.class);
        startActivity(intent);
    }


}
